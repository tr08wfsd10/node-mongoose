var mongoose = require('mongoose');
mongoose.connect('mongodb://localhost:27017/test');

var db = mongoose.connection;
db.on('error',console.error.bind(console, 'conection error:'));

db.once('open', function(){
	console.log('hola');
});

var userSchema = new mongoose.Schema({
	nombre: String,
	apellido: String,
	mascota: String

});

// var Usuarios es mi base de datos, y mi usuarios entre comillas en la colección 
var Usuario = mongoose.model(
	'Usuario', userSchema
);

//var paco = new Usuario({nombre:'Paco', apellido:'Pérez', mascota:'perro'});

module.exports = Usuario;