var db = {
		 };

const express = require('express');

var app = express();

var bodyParser = require('body-parser');

var Usuario = require('./db.js');


app.use(express.static(__dirname + '/user'));


app.use(bodyParser.json());


// CRUD
app.get('/users',(req, res) => { 
 	Usuario.find({}, null, null,(err, users) => {
    if (err || !users){
      console.log("No ha sido posible acceder a la base de datos");
    }res.send(users)

	})
});

app.get ('/user/:id', (req,res) => {
	
	if (!req.params.id) { 

		return res.status(500).send("Petición inválida, es necesario el ID")
	}

	Usuario.findById (req.params.id, function (error, user) {

        if (error || !user) { 

        	return res.status(401).send("No estás autorizado");

        } else {
        	
        	return res.status(200).send(user);
        }
    })
});

app.post('/user', (req,res) => {
	var usuarios = new Usuario({
		nombre:req.body.nombre,
		apellido:req.body.apellido,
		mascota:req.body.mascota
	})

	usuarios.save( err => {

		if (err) return console.log("No se ha conectado")});
	
	res.send(`Se ha guardado el nuevo ${usuarios.nombre} y el nuevo ${usuarios.apellido} y la nueva ${usuarios.mascota}`);
});

app.put('/user/:id', (req,res) => {

	Usuario.findById (req.params.id, function (error, user) {

		if(error || !user)
		{
			return res.status(401).send("No puedes pasaaaaaaar");
		}
		
		if (req.body.nombre) {
			user.nombre=req.body.nombre;
		}

		if (req.body.apellido) {
			user.apellido=req.body.apellido;
		}

		if (req.body.mascota) {
			user.mascota=req.body.mascota;
		}

		user.save((err) => {
			if (err){ 
				return res.send("No se ha conectado");
			} else {
				res.send(user);
			}
		});
	});
});

app.delete('/user/:id', (req,res) => {
	Usuario.remove (req.params.id, function (error) {
        if (error){
          res.send("No se ha borrado");
        }else{
        	res.send("Se ha borrado con éxito");
        }
	
	});
});


app.listen(3000);